<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		$this->load->model('M_kecamatan');
		$this->load->model('M_kota');
	}
	public function index(){
		$data['judul'] = "Data Kecamatan";
		$data['listKecamatan'] = $this->M_kecamatan->getKecamatan();

		$this->template->load('tamplate/v_index','kecamatan/v_kecamatan', $data);

	}
	public function hapusKecamatan($id){
		$data = array('id_kecamatan' => $id );
		$this->M_kecamatan->deletekecamatana($data); 
		$this->session->set_flashdata('flashKecamatan', 'Data Kecamatan Berhasil Dihapus');
        echo json_encode(array("status" => TRUE));
	}
	
	public function tambahKecamatan(){
		$data['judul'] = "Tambah Kecamatan"; 

		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data['listKota'] = $this->M_kota->getKota();
			$this->template->load('tamplate/v_index','kecamatan/v_tambah', $data);
		} else{
			$kota = $this->input->post('kota');
			$kecamatan = $this->input->post('kecamatan');

			$data = array(
							'kecamatan' => $kecamatan,
							'kota' => $kota
						 );
			$this->M_kecamatan->simpanKecamatan($data);
			$this->session->set_flashdata('flashKota', 'Data kecamatan Berhasil Ditambah');
			redirect(base_url('kecamatan'),'refresh');
		}

	}


	public function editKecamatan($id){
		$data['judul'] = "Edit Kecamatan"; 

		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data['listKota'] = $this->M_kota->getKota();
			$data['kec'] = $this->M_kecamatan->getKecamatanId($id);
			$this->template->load('tamplate/v_index','kecamatan/v_edit', $data);
		} else{

			$id_kecamatan = $this->input->post('id_kecamatan');
			$kota = $this->input->post('kota');
			$kecamatan = $this->input->post('kecamatan');

			$data = array(
							'kecamatan' => $kecamatan,
							'kota' => $kota
						 );

			$id = array('id_kecamatan' => $id_kecamatan);

			$this->M_kecamatan->editKecamatan($id,$data);
			$this->session->set_flashdata('flashKecamatan', 'Data kecamatan Berhasil Diedit');
			redirect(base_url('kecamatan'),'refresh');
		}

	}
}