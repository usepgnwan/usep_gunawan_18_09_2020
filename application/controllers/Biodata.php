<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('M_biodata');		
		$this->load->model('M_kota');
	}

	public function index()
	{
		$data['judul'] = "Biodata Siswa"; 
		$data['data_siswa'] = $this->M_biodata->getSiswa(); 
		$this->template->load('tamplate/v_index','biodata/v_biodata', $data);
	}

	public function tambahSiswa(){
		$data['judul'] = "Tambah Siswa"; 

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('kota', 'Kota/Kabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data['listKota'] = $this->M_kota->getKota();
			$this->template->load('tamplate/v_index','biodata/v_tambah', $data);
		} else{
			
			$nama = $this->input->post('nama');
			$kota = $this->input->post('kota');
			$kecamatan = $this->input->post('kecamatan');
			$alamat = $this->input->post('alamat');

			$data = array(
							'nama_siswa' => $nama,
							'kota' => $kota,
							'kecamatan' => $kecamatan,
							'alamat' => $alamat
						 );
			$this->M_biodata->simpanSiswa($data);
			$this->session->set_flashdata('flashSiswa', 'Data Siswa Berhasil Ditambah');
			redirect(base_url('biodata'),'refresh');
		}

	}

	public function hapusSiswa($id){
		$data = array('id_siswa' => $id );
		$this->M_biodata->deleteSiswa($data); 
		$this->session->set_flashdata('flashSiswa', 'Data Siswa Berhasil Dihapus');
        echo json_encode(array("status" => TRUE));
	}

	public function editSiswa($id){
		$data['judul'] = "Edit Siswa"; 
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('kota', 'Kota/Kabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');

		if ($this->form_validation->run() == FALSE) {
			
			$data['dataSiswa'] = $this->M_biodata->getSiswaId($id);
			$data['listKota'] = $this->M_kota->getKota();
			$this->template->load('tamplate/v_index','biodata/v_edit', $data);
		} else{
			
			$nama = $this->input->post('nama');
			$kota = $this->input->post('kota');
			$kecamatan = $this->input->post('kecamatan');
			$alamat = $this->input->post('alamat');
			$id_siswa = $this->input->post('id_siswa');

			$data = array(
							'nama_siswa' => $nama,
							'kota' => $kota,
							'kecamatan' => $kecamatan,
							'alamat' => $alamat
						 );
			$id = array('id_siswa' => $id_siswa  );
			$this->M_biodata->editSiswa($id,$data);
			$this->session->set_flashdata('flashSiswa', 'Data Siswa Berhasil Diedit');
			redirect(base_url('biodata'),'refresh');
		}

	}	
}
