<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller {
	public function __construct(){
		parent::__construct();	
		$this->load->model('M_kota');
	}
	public function index(){
		$data['judul'] = "Data Kota/Kabupaten";
		$data['listKota'] = $this->M_kota->getKota();
		$this->template->load('tamplate/v_index','kota/v_kota', $data);

	}
	public function getKecamatan($id){
		$data = $this->M_kota->getKecamatanId($id);
		echo json_encode($data);	
	}

	public function hapusKota($id){
		$data = array('id_kota' => $id );
		$this->M_kota->deletekota($data); 
		$this->session->set_flashdata('flashKota', 'Data Kota Berhasil Dihapus');
        echo json_encode(array("status" => TRUE));
	}

	public function tambahKota(){
		$data['judul'] = "Tambah Kota"; 

		$this->form_validation->set_rules('kota', 'kota', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('tamplate/v_index','kota/v_tambah', $data);
		} else{
			
			$kota = $this->input->post('kota');

			$data = array(
							
							'kota' => $kota
						 );
			$this->M_kota->simpanKota($data);
			$this->session->set_flashdata('flashKota', 'Data Kota Berhasil Ditambah');
			redirect(base_url('kota'),'refresh');
		}

	}

	public function editKota($id){
		$data['judul'] = "Edit Kota"; 

		$this->form_validation->set_rules('kota', 'kota', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data['dataKota'] = $this->M_kota->getKotaId($id);
			
			$this->template->load('tamplate/v_index','kota/v_edit', $data);
		} else{
			
			$id_kota = $this->input->post('id_kota');
			$kota = $this->input->post('kota');
			$id = array('id_kota' => $id_kota );
			$data = array(
							'kota' => $kota
						 );
			$this->M_kota->editKota($id,$data);
			
			$this->session->set_flashdata('flashKota', 'Data Kota Berhasil Diedit');
			redirect(base_url('kota'),'refresh');
		}

	}

}