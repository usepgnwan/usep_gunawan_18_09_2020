<div class="row justify-content-center">
	<div class="col-md-6 mt-3">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title"><?= $judul ?></h5>
		    <form method="post" action="">

		    <?php foreach ($dataSiswa as $key): ?>
			  <div class="form-group">
			  <input type="hidden" class="form-control"  placeholder="Masukan Nama" name="id_siswa" value="<?= $key['id_siswa'] ?>">

			    <label for="inputAddress">Nama</label>
			    <input type="input" class="form-control"  placeholder="Masukan Nama" name="nama" value="<?= $key['nama_siswa'] ?>">
			     <small id="emailHelp" class="form-text text-muted"><?= form_error('nama'); ?></small>
			  </div>

		    <?php endforeach ?>		
			  <div class="form-group">
			     <label for="inputState">Kota/kabupaten</label>
			      <select class="form-control" name="kota" id="e_kota">
			        <option selected>Pilih Kota/Kabupaten</option>
			        		        	
				    <option value="<?= $key['id_kota'] ?>" selected><?= $key['kota'] ?></option>
				    <?php foreach ($listKota as $key):?>			        	
				        <option value="<?= $key['id_kota'] ?>"><?= $key['kota'] ?></option>
			        <?php endforeach ?>
			      </select>
			       <small id="emailHelp" class="form-text text-muted"><?= form_error('kota'); ?></small>
			  </div>
			 <?php foreach ($dataSiswa as $key): ?>
			 <div class="form-group" id="kec">
			     <label for="inputState">Kecamatan</label>
			      <select class="form-control" name="kecamatan" id="e_kc">
			       <option value="<?= $key['id_kecamatan'] ?>" selected><?= $key['kecamatan'] ?></option>
			      </select>
			       <small id="emailHelp" class="form-text text-muted"><?= form_error('kota'); ?></small>
			  </div>    
			  <?php endforeach ?>
		    <?php foreach ($dataSiswa as $key): ?>	  	  
			  <div class="form-group">
			    <label for="inputAddress">Alamat</label>
			    <textarea class="form-control" id="alamat" name="alamat" rows="4" cols="50"><?= $key['alamat'] ?></textarea>
				 <small id="emailHelp" class="form-text text-muted"><?= form_error('alamat'); ?></small>
			  </div>		
			<?php endforeach ?>
			  <button type="submit" class="btn btn-primary">Simpan</button>
			</form>
		  </div>
		</div>
	</div>			
</div>
