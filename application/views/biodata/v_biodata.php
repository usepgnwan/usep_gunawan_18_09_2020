<div class="row mb-3 mt-3">
	<div class="col"><a href="<?php echo base_url('biodata/tambahsiswa')?>" class="btn btn-primary">Tambah Siswa</a></div>
</div>
<?php if ($this->session->flashdata('flashSiswa')): ?>
	<div class="row">
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		 <strong><?php echo $this->session->flashdata('flashSiswa'); ?></strong> 
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
	</div>
<?php endif ?>

<div class="col-md-12">
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Nama Siswa</th>
	      <th scope="col">Kota/Kabupaten</th>
	      <th scope="col">Kecamatan</th>
	      <th scope="col">Alamat</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  <?php $no = 1; foreach ($data_siswa as $key): ?>
	  	 <tr>
	      <th scope="row"><?= $no++ ?></th>
	      <td><?= $key['nama_siswa'] ?></td>
	      <td><?= $key['kota'] ?></td>
	      <td><?= $key['kecamatan'] ?></td>
	      <td><?= $key['alamat'] ?></td>
	      <td>
	      		<a href="<?php  echo  base_url('Biodata/editSiswa/') ?><?= $key['id_siswa'] ?> "><span class="badge badge-secondary">Edit</span></a>
	      		<a href="#" onclick="hapusSiswa(<?= $key['id_siswa'] ?>)"><span class="badge badge-danger">Delete</span></a>
	      </td>
	    </tr>
	  <?php endforeach ?>
	   
	   
	  </tbody>
	</table>
</div>