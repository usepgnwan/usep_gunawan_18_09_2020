<div class="row justify-content-center">
	<div class="col-md-6 mt-3">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title"><?= $judul ?></h5>
		    <form method="post" action="">
		    <?php foreach ($dataKota as $key): ?>			    
				  <div class="form-group">
				    <input type="hidden" class="form-control"  placeholder="Masukan kota" name="id_kota" value="<?= $key['id_kota'] ?>">
				    <label for="inputAddress">Kota</label>
				    <input type="input" class="form-control"  placeholder="Masukan kota" name="kota" value="<?= $key['kota'] ?>">
				     <small id="emailHelp" class="form-text text-muted"><?= form_error('kota'); ?></small>
				  </div>	
		    <?php endforeach ?>
			  <button type="submit" class="btn btn-primary">Simpan</button>
			</form>
		  </div>
		</div>
	</div>			
</div>
