<div class="row justify-content-center">
	<div class="col-md-6 mt-3">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title"><?= $judul ?></h5>
		    <form method="post" action="">

		     <?php foreach ($kec as $key):?>
				  <div class="form-group">
				    <label for="inputAddress">Kecamatan</label>
				    <input type="hidden" class="form-control"  placeholder="Masukan Kecamatan" name="id_kecamatan" value="<?= $key['id_kecamatan'] ?>">

				    <input type="input" class="form-control"  placeholder="Masukan Kecamatan" name="kecamatan" value="<?= $key['kecamatan'] ?>">
				     <small id="emailHelp" class="form-text text-muted"><?= form_error('kecamatan'); ?></small>
				  </div>		        	 
		        <?php endforeach ?>
			   <div class="form-group">
			     <label for="inputState">Kota/kabupaten</label>
			      <select class="form-control" name="kota" id="kota">

			        <?php foreach ($kec as $key):?>
			        	 <option value="<?= $key['id_kota'] ?>" selected><?= $key['kota'] ?></option>
			        <?php endforeach ?>
			        <?php foreach ($listKota as $key):?>				        	
				        <option value="<?= $key['id_kota'] ?>"><?= $key['kota'] ?></option>
			        <?php endforeach ?>
			      </select>
			       <small id="emailHelp" class="form-text text-muted"><?= form_error('kota'); ?></small>
			  </div>
			  <button type="submit" class="btn btn-primary">Simpan</button>
			</form>
		  </div>
		</div>
	</div>			
</div>
