<div class="row mb-3 mt-3">
	<div class="col"><a href="<?php echo base_url('kecamatan/tambahKecamatan')?>" class="btn btn-primary">Tambah Kecamatan</a></div>
</div>

<?php if ($this->session->flashdata('flashKecamatan')): ?>
	<div class="row">
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		 <strong><?php echo $this->session->flashdata('flashKecamatan'); ?></strong> 
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
	</div>
<?php endif ?>

<div class="col-md-12">
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Kecamatan</th>
	      <th scope="col">Kota</th>	      
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  <?php $no = 1; foreach ($listKecamatan as $key): ?>
	  	 <tr>
	      <th scope="row"><?= $no++ ?></th>
	      <td><?= $key['kecamatan'] ?></td>
	      <td><?= $key['kota'] ?></td>
	      <td>
	      		<a href="<?php  echo  base_url('kecamatan/editkecamatan/') ?><?= $key['id_kecamatan'] ?> "><span class="badge badge-secondary">Edit</span></a>
	      		<a href="#" onclick="hapusKecamatan(<?= $key['id_kecamatan'] ?>)"><span class="badge badge-danger">Delete</span></a>
	      </td>
	    </tr>
	  <?php endforeach ?>
	   
	   
	  </tbody>
	</table>
</div>