<!DOCTYPE html>
<html>
<head>
	<title><?= $judul ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css');?>">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container">
 	
  <a class="navbar-brand" href="#">Seon Test</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">  
	    <div class="navbar-nav">
	      <a class="nav-item nav-link active" href="<?php echo base_url('biodata'); ?>">Biodata Siswa <span class="sr-only">(current)</span></a>
	      <a class="nav-item nav-link" href="<?php echo base_url('kota'); ?>">Kabupaten/Kota</a>
	      <a class="nav-item nav-link" href="<?php echo base_url('kecamatan'); ?>">Kecamatan</a>
	    </div>
  </div>
 </div>
</nav>
<div class="container">
	<?php echo $contents;?>
</div>
<script type="text/javascript" src="<?php echo base_url('asset/jquery-3.5.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js');?>"></script>
</body>

<script type="text/javascript">

//*Bagian Biodata*/
	function hapusSiswa(id){
		if (confirm("yakin hapus Siswa?")) {

			$.ajax({
				url:'<?php echo base_url("biodata/hapusSiswa/"); ?>'+id,
				type:"POST",
				dataType:"JSON",
				success:function(){ 
                    location.reload();
				},
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error Get/Delete data from ajax');
		        }
			});
		}
	}
	$( document ).ready(function() {
		$("#kecamatan").hide();
		$('#kota').change(function(){ 
	   			const id = $(this).val();

	   			$("#kecamatan").show();
					$.ajax({
						url:'<?php echo base_url("kota/getKecamatan/"); ?>'+id,
						type:"POST",
						dataType:"JSON",
						success:function(result){ 
							console.log(result);
		                   $.each(result, function(i,data){
		                   		
		                   		$('#kc').append(`<option value="${data.id_kecamatan}">${data.kecamatan}</option>`);
		                   });
						},
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error Get/Delete data from ajax');
				        }
					});
	   		
	   		
		});

		$('#e_kota').change(function(){ 
	   			const id = $(this).val();

	   			
					$.ajax({
						url:'<?php echo base_url("kota/getKecamatan/"); ?>'+id,
						type:"POST",
						dataType:"JSON",
						success:function(result){ 
		                   $.each(result, function(i,data){
		                   		
		                   		$('#e_kc').append(`<option value="${data.id_kecamatan}">${data.kecamatan}</option>`);
		                   });
						},
				        error: function (jqXHR, textStatus, errorThrown)
				        {
				            alert('Error Get/Delete data from ajax');
				        }
					});
	   		
	   		
		});
	});

	//*Penutup Bagian Biodata*/
		function hapusKota(id){
			if (confirm("yakin hapus Kota/Kabupaten?")) {

				$.ajax({
					url:'<?php echo base_url("kota/hapusKota/"); ?>'+id,
					type:"POST",
					dataType:"JSON",
					success:function(){ 
	                    location.reload();
					},
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error Get/Delete data from ajax');
			        }
				});
			}
		}
	//*Bagian Kota*/
		//*Penutup Bagian Biodata*/
		function hapusKecamatan(id){
			if (confirm("yakin hapus Kecamatan?")) {

				$.ajax({
					url:'<?php echo base_url("kecamatan/hapusKecamatan/"); ?>'+id,
					type:"POST",
					dataType:"JSON",
					success:function(){ 
	                    location.reload();
					},
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error Get/Delete data from ajax');
			        }
				});
			}
		}
	//*Bagian Kota*/
</script>
</html>