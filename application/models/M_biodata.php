<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_biodata extends CI_Model {
	public function getSiswa(){
		$sql = 'SELECT tb_siswa.nama_siswa, tb_siswa.alamat, tb_siswa.id_siswa ,tb_kota.kota, tb_kecamatan.kecamatan from tb_siswa JOIN tb_kota on tb_siswa.kota = tb_kota.id_kota JOIN tb_kecamatan on tb_siswa.kecamatan = tb_kecamatan.id_kecamatan order by tb_siswa.id_siswa Desc';
		return $this->db->query($sql)->result_array();
	}

	public function getSiswaId($id){
		$sql = "SELECT tb_siswa.nama_siswa, tb_siswa.alamat, tb_siswa.id_siswa ,tb_kota.id_kota, tb_kota.kota,tb_kecamatan.id_kecamatan,tb_kecamatan.kecamatan from tb_siswa JOIN tb_kota on tb_siswa.kota = tb_kota.id_kota JOIN tb_kecamatan on tb_siswa.kecamatan = tb_kecamatan.id_kecamatan where tb_siswa.id_siswa = '$id'";
		return $this->db->query($sql)->result_array();
	}

	public function simpanSiswa($data){
		return $this->db->insert('tb_siswa', $data);
	}

	public function deleteSiswa($id){
		return $this->db->delete('tb_siswa', $id);
	}

	public function editSiswa($id,$data){
		$this->db->where($id);
		return $this->db->update('tb_siswa', $data);
	}
}