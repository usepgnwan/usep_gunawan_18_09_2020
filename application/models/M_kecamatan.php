<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kecamatan extends CI_Model {

	public function getKecamatan(){
		$sql = 'select * from tb_kecamatan join tb_kota on tb_kecamatan.kota = tb_kota.id_kota';
		return $this->db->query($sql)->result_array();
	}
	public function getKecamatanId($id){
		$sql = "select * from tb_kecamatan join tb_kota on tb_kecamatan.kota = tb_kota.id_kota where tb_kecamatan.id_kecamatan = '$id'";
		return $this->db->query($sql)->result_array();
	}
	public function deletekecamatana($id){
		return $this->db->delete('tb_kecamatan',$id);
	}
	public function simpanKecamatan($data){
		return $this->db->insert('tb_kecamatan', $data);
	}

	public function editKecamatan($id, $data){
		$this->db->where($id);
		return $this->db->update('tb_kecamatan', $data);
	}

}