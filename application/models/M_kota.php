<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kota extends CI_Model {
	public function getKota(){
		$this->db->select('*');
		$this->db->from('tb_kota');
		return $this->db->get()->result_array();
	}
	public function getKotaId($id){
	
		$this->db->from('tb_kota');
		$this->db->where('id_kota',$id);
		return $this->db->get()->result_array();
	}
	public function getKecamatanId($id){
		$sql = "SELECT * FROM tb_kecamatan JOIN tb_kota ON tb_kota.id_kota = tb_kecamatan.kota WHERE tb_kota.id_kota = '$id'";
		return $this->db->query($sql )->result_array();
	}

	public function deletekota($id){
		return $this->db->delete('tb_kota',$id);
	}

	public function simpanKota($data){
		return $this->db->insert('tb_kota', $data);
	}

	public function editKota($id, $data){
		$this->db->where($id);
		return $this->db->update('tb_kota', $data);
	}

}